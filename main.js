const newTable = document.createElement('table');
let num = 1;

for (let i = 0; i < 10; i++) {
	const tr = document.createElement('tr');
	newTable.append(tr);
	for (let i = 0; i < 10; i++) {
		const td = document.createElement('td');
		tr.append(td);
		td.innerHTML = num++;
	}
}

document.body.append(newTable);
